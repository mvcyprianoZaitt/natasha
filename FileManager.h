#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <QString>

#include "file_system/JSonFile.h"

struct RestConfig {
    std::string base_url;
    std::string authorization;
    std::string content_type;
};

class FileManager {

 public:
     FileManager();
     virtual ~FileManager();
     RestConfig GetRestConfiguration();

 private:
     void ParseJson();
     natasha::file_system::JSonFile json_file_;
     RestConfig rest_config_;
};

#endif
