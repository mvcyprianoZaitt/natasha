#include <QCoreApplication>
#include <QObject>

#include <memory>
#include <string>

#include "defines.h"
#include "FileManager.h"
#include "spdlog/spdlog.h"
#include "rest_api/RestClient.h"

using natasha::rest_api::RestClient;

int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);
    std::shared_ptr<spdlog::logger> logger = spdlog::basic_logger_mt(
            "natasha.log",
            LOG_FILE, false);
    logger->flush_on(spdlog::level::info);
    logger->info("Starting restclient application.");
    FileManager file_manager = FileManager();
    std::shared_ptr<RestClient> rest_client =
            std::make_shared<RestClient>(logger,
                    file_manager.GetRestConfiguration());
	return app.exec();
}
