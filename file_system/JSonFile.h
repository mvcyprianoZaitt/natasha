#ifndef JSonFile_H
#define JSonFile_H
#include <QString>
#include <QFile>
#include <QIODevice>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

namespace natasha {
namespace file_system {

class JSonFile {

  public:
    JSonFile(QString path);
    virtual ~JSonFile();
    void WriteFileJson(QString file_name);
    QJsonObject GetObject();

  private:
    void ReadFileJson();
    QJsonObject json_object_;
    QString read_path_;
};

}
}

#endif
