#include "JSonFile.h"

namespace natasha {
namespace file_system {
JSonFile::JSonFile(QString path){
    read_path_ = path;
    ReadFileJson();
}

JSonFile::~JSonFile(){

}

void JSonFile::ReadFileJson(){
    QString file_string;
    QFile file;
    file.setFileName(read_path_);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    file_string = file.readAll();
    file.close();
    QJsonDocument json_document = QJsonDocument::fromJson(file_string.toUtf8());
    json_object_ = json_document.object();
}

void JSonFile::WriteFileJson(QString file_name){
    QJsonDocument json_document(json_object_);
    QFile jsonFile(file_name);
    jsonFile.open(QFile::WriteOnly);
    jsonFile.write(json_document.toJson());
}

QJsonObject JSonFile::GetObject(){
    return json_object_;
}

}
}
